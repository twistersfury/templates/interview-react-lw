import { defineConfig } from 'cypress'

export default defineConfig({
    e2e: {
        baseUrl: 'http://host.docker.internal:3000',
        defaultCommandTimeout: 10000,
        excludeSpecPattern: 'cache/cy-press'
    },
    component: {
        devServer: {
            framework: 'next',
            bundler: 'webpack'
        },
        excludeSpecPattern: 'cache/cy-press'
    }
})