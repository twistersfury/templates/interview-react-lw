class Logger {
    debug(message: string, context: object) {
        if (this.isEnabled()) {
            console.log(message, context);
        }
    }

    isEnabled() {
        //TODO: Convert To Global Environment Variable

        return true;
    }
}

const loggerInstance = new Logger();

export default loggerInstance;