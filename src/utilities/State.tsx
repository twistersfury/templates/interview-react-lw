import {createContext, PropsWithChildren, useReducer} from "react";
import logger from "./Logger";
import Character from "../models/Character";

interface IAction {
    type: string;
    value: string;
}

interface IState {
    searchValue: string;

    currentCharacter?: Character;
}

const initialState = {
    searchValue: '',
    currentCharacter: null
};

const appStore = createContext(initialState);

const { Provider } = appStore;

const StateProvider = (props: PropsWithChildren) => {
    const [state, dispatch] = useReducer(
        (state: IState|any, action: IAction) => {
            logger.debug("State Dispatched", { action: action, state: state });

            switch(action.type) {
                case 'search/update':
                    return {
                        searchValue: action.value,
                        currentCharacter: state.currentCharacter
                    };
                case 'character':
                    return {
                        searchValue: state.searchValue,
                        currentCharacter: action.value
                    };
                default:
                    throw new Error("Invalid Action: " + action.type);
            }
        },
        initialState
    );

    return <Provider value={{ searchValue: state.searchValue, dispatch }}>{props.children}</Provider>;
};

export { appStore, StateProvider }