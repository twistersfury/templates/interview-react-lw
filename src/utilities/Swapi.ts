import Character from "../models/Character";
import Species from "../models/Species";
import Starship from "../models/Starship";
import logger from "../utilities/Logger";
import Film from "../models/Film";

export class Swapi {
    readonly baseUri: string = "https://swapi.dev/api"

    //Caching For Previous Promises
    private requests: Map<string, Promise<Response>> = new Map();

    makeRequest(url: string): any {
        let fullUrl = this.buildUri(url);

        if (!this.requests.has(fullUrl)) {
            this.requests.set(
                fullUrl,
                fetch(this.buildUri(url)).then(res => res.json()).then((data) => {
                    logger.debug(
                        "Make Request",
                        {
                            url: url,
                            results: data
                        }
                    );

                    return data;
                })
            )
        }

        return this.requests.get(fullUrl);
    }

    buildUri(url: string) {
        return this.baseUri + url;
    }

    searchCharacters(search: string): Promise<Array<Character>> {
        logger.debug("Search Characters", {search: search});

        return this.makeRequest(
            "/people/?search=" + search
        ).then((data: any) => {
            return data.results.map((item: any) => {
                return new Character(item);
            });
        });
    }

    loadCharacter(id: string): Promise<Character> {
        logger.debug("Load Character", {id: id});

        return this.makeRequest(
            "/people/" + id,
        ).then((data: any) => {
            return new Character(data);
        });
    }

    loadSpecies(id: string): Promise<Species> {
        logger.debug("Load Species", {id: id});

        return this.makeRequest(
            "/species/" + id
        ).then((data: any) => {
            return new Species(data);
        });
    }

    loadStarship(id: string): Promise<Starship> {
        logger.debug("Load Starship", {id: id});

        return this.makeRequest(
            "/starships/" + id
        ).then((data: any) => {
            return new Starship(data);
        });
    }

    loadFilm(id: string): Promise<Film> {
        logger.debug("Load Film", {id: id});

        return this.makeRequest(
            "/films/" + id
        ).then((data: any) => {
            return new Film(data);
        });
    }
}

const defaultInstance = new Swapi();

export default defaultInstance;