class DummyClass {
    private readonly value: String;
    constructor(initValue: String) {
        this.value = initValue
    }

    public getValue(): String
    {
        return this.value;
    }
}

export default DummyClass;