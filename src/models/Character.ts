import Species from "./Species";
import swapi from "../utilities/Swapi";
import Starship from "./Starship";
import logger from "../utilities/Logger";
import Film from "./Film";

interface ICharacter {
    name: string;
    height: string;
    mass: string;
    hair_color: string;
    birth_year: string;
    url: string;
    species: Array<string>;
    starships: Array<string>;
    films: Array<string>;
}

class Character {
    private _rawData: ICharacter;

    constructor(rawData: ICharacter) {
        logger.debug("Character Construct", {
            rawData: rawData
        });

        this._rawData = rawData;
    }

    public get name(): string {
        return this._rawData.name;
    }

    public get id(): string {
        return this._rawData.url.replace(/[^0-9]+/, '');
    }

    public get height(): string {
        return this._rawData.height;
    }

    public get weight(): string {
        return this._rawData.mass;
    }

    public get hairColor(): string {
        return this._rawData.hair_color;
    }

    public get birthDate(): string {
        return this._rawData.birth_year;
    }

    getSpecies(): Promise<Array<Species>> {
        let waitAll: Array<Promise<Species>> = [];

        logger.debug("Species", this._rawData.species);

        for (const key in this._rawData.species) {
            waitAll.push(swapi.loadSpecies(this._rawData.species[key].replace(/[^0-9]+/, '')));
        }

        return Promise.all(waitAll);
    }

    getStarships(): Promise<Array<Starship>> {
        let waitAll: Array<Promise<Starship>> = [];

        logger.debug("Starships", this._rawData.starships);

        for (const key in this._rawData.starships) {
            waitAll.push(swapi.loadStarship(this._rawData.starships[key].replace(/[^0-9]+/, '')));
        }

        return Promise.all(waitAll);
    }

    getFilms(): Promise<Array<Film>> {
        let waitAll: Array<Promise<Film>> = [];

        logger.debug("Films", this._rawData.films);

        for (const key in this._rawData.films) {
            waitAll.push(swapi.loadFilm(this._rawData.films[key].replace(/[^0-9]+/, '')));
        }

        return Promise.all(waitAll);
    }
}

export default Character;