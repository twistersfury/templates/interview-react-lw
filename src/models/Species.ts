interface ISpecies {
    name: string,
    classification: string
}

class Species {
    private _rawData: ISpecies;

    constructor(rawData: ISpecies) {
        this._rawData = rawData;
    }

    public get name(): string {
        return this._rawData.name;
    }

    public get classification(): string {
        return this._rawData.classification;
    }
}

export default Species;