
interface IStarship {
    name: string,
    model: string;
}

class Starship {
    private _rawData: IStarship;

    constructor(rawData: IStarship) {
        this._rawData = rawData;
    }

    public get name(): string {
        return this._rawData.name;
    }

    public get model(): string {
        return this._rawData.model;
    }
}

export default Starship;