interface IFilm {
    title: string
}

class Film {
    private _rawData: IFilm;

    constructor(rawData: IFilm) {
        this._rawData = rawData;
    }

    public get title(): string {
        return this._rawData.title;
    }
}

export default Film;