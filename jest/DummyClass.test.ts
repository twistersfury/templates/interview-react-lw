import {expect} from "@jest/globals";
import DummyClass from "./../src/DummyClass";
describe('Testing DummyClass', () => {
    test("Checking Get Value", () => {
        const instance = new DummyClass("something");
        expect(instance.getValue()).toEqual("something");
    });
});