import {jest, expect} from "@jest/globals";
import Character from "../../src/models/Character";
import Film from "../../src/models/Film";
import Starship from "../../src/models/Starship";
import Species from "../../src/models/Species";

describe('Testing Character', () => {
    let testSubject: Character;

    beforeEach(() => {
        jest.spyOn(console, 'log').mockImplementation(() => {});

        testSubject = new Character({
            name: "Something",
            height: "height",
            mass:"weight",
            hair_color:"hair color",
            birth_year:"birth",
            url: "https://swapi.dev/api/people/20",
            species: [
                "https://swapi.dev/api/species/1",
                "https://swapi.dev/api/species/2"
            ],
            starships: [
                "https://swapi.dev/api/starships/1",
                "https://swapi.dev/api/starships/2",
                "https://swapi.dev/api/starships/3",
            ],
            films: [
                "https://swapi.dev/api/films/1",
                "https://swapi.dev/api/films/2",
                "https://swapi.dev/api/films/3",
                "https://swapi.dev/api/films/4",
            ]
        });
    });

    test("it gets the id", () => {
        expect(testSubject.id).toEqual("20");
    });

    test("it gets the name", () => {
        expect(testSubject.name).toEqual("Something");
    })

    test("it gets the height", () => {
        expect(testSubject.height).toEqual("height");
    })

    test("it gets the weight", () => {
        expect(testSubject.weight).toEqual("weight");
    })

    test("it gets the hairColor", () => {
        expect(testSubject.hairColor).toEqual("hair color");
    })

    test("it gets the birthDate", () => {
        expect(testSubject.birthDate).toEqual("birth");
    })

    test("it gets the films", () => {
        jest.mock("../../src/utilities/Swapi", () => ({
            loadFilm: () => new Promise((resolve) => {
                resolve(new Film({title: "Title"}));
            })
        }));

        return testSubject.getFilms().then((data) => {
            expect(data.length).toEqual(4);
        });
    });

    test("it gets the starships", () => {
        jest.mock("../../src/utilities/Swapi", () => ({
            loadFilm: () => new Promise((resolve) => {
                resolve(new Starship({name: "name", model: "model"}));
            })
        }));

        return testSubject.getStarships().then((data) => {
            expect(data.length).toEqual(3);
        });
    });

    test("it gets the species", () => {
        jest.mock("../../src/utilities/Swapi", () => ({
            loadFilm: () => new Promise((resolve) => {
                resolve(new Species({name: "name", classification: "classification"}));
            })
        }));

        return testSubject.getSpecies().then((data) => {
            expect(data.length).toEqual(2);
        });
    });
});