import {jest, expect} from "@jest/globals";
import Film from "../../src/models/Film";
jest.mock('../../src/utilities/Swapi', () => {
    const originalModule = jest.requireActual<object>('../../src/utilities/Swapi');

    return {
        __esModule: true,
        ...originalModule,
        makeRequest: () => jest.fn()
    };
});

jest.spyOn(console, 'log');

describe('Testing Film', () => {
    let testSubject: Film;

    beforeEach(() => {
        testSubject = new Film({
            title: "title"
        });
    });

    test("it gets the id", () => {
        expect(testSubject.title).toEqual("title");
    });
});