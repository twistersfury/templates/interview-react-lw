import {jest, expect} from "@jest/globals";
import Species from "../../src/models/Species";
jest.mock('../../src/utilities/Swapi', () => {
    const originalModule = jest.requireActual<object>('../../src/utilities/Swapi');

    return {
        __esModule: true,
        ...originalModule,
        makeRequest: () => jest.fn()
    };
});

jest.spyOn(console, 'log');

describe('Testing Species', () => {
    let testSubject: Species;

    beforeEach(() => {
        testSubject = new Species({
            name: "name",
            classification: "classification"
        });
    });

    test("it gets the name", () => {
        expect(testSubject.name).toEqual("name");
    });

    test("it gets the classification", () => {
        expect(testSubject.classification).toEqual("classification");
    });
});