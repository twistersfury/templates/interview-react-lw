import {jest, expect} from "@jest/globals";
import Starship from "../../src/models/Starship";
jest.mock('../../src/utilities/Swapi', () => {
    const originalModule = jest.requireActual<object>('../../src/utilities/Swapi');

    return {
        __esModule: true,
        ...originalModule,
        makeRequest: () => jest.fn()
    };
});

jest.spyOn(console, 'log');

describe('Testing Starship', () => {
    let testSubject: Starship;

    beforeEach(() => {
        testSubject = new Starship({
            name: "name",
            model: "model"
        });
    });

    test("it gets the name", () => {
        expect(testSubject.name).toEqual("name");
    });

    test("it gets the classification", () => {
        expect(testSubject.model).toEqual("model");
    });
});