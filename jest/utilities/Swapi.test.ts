import {jest, expect} from "@jest/globals";
import { Swapi } from "../../src/utilities/Swapi";
import Film from "../../src/models/Film";
import Starship from "../../src/models/Starship";
import Species from "../../src/models/Species";
import Character from "../../src/models/Character";

describe("Testing SWAPI", () => {
    let testSubject: Swapi;

    beforeEach(() => {
        jest.spyOn(console, 'log').mockImplementation(() => {});

        testSubject = new Swapi();
    });

    test("it can load film", async () => {
        global.fetch = jest.fn<any>(() => Promise.resolve({
                json: () => Promise.resolve({
                    title: "my-title"
                }),
            })
        );

        const film: Film = await testSubject.loadFilm("1");

        expect(film.title).toEqual("my-title");
    });

    test("it can load starship", async () => {
        global.fetch = jest.fn<any>(() => Promise.resolve({
                json: () => Promise.resolve({
                    name: "name",
                    model: "model"
                }),
            })
        );

        const starship: Starship = await testSubject.loadStarship("1");

        expect(starship.name).toEqual("name");
        expect(starship.model).toEqual("model");
    });

    test("it can load species", async () => {
        global.fetch = jest.fn<any>(() => Promise.resolve({
                json: () => Promise.resolve({
                    name: "name",
                    classification: "classification"
                }),
            })
        );

        const species: Species = await testSubject.loadSpecies("1");

        expect(species.name).toEqual("name");
        expect(species.classification).toEqual("classification");
    });

    test("it can load character", async () => {
        global.fetch = jest.fn<any>(() => Promise.resolve({
                json: () => Promise.resolve({
                    name: "my-name"
                }),
            })
        );

        const character: Character = await testSubject.loadCharacter("1");

        expect(character.name).toEqual("my-name");
    });

    test("it can search characters", async() => {
        global.fetch = jest.fn<any>(() => Promise.resolve({
                json: () => Promise.resolve({results: [{
                    name: "my-name"
                },{
                    name: "other-name"
                }]}),
            })
        );

        const characters: Array<Character> = await testSubject.searchCharacters("search");

        expect(characters[0].name).toEqual("my-name");
        expect(characters[1].name).toEqual("other-name");
    });
});

export {};