describe('Workflow', () => {
    it ('should test search', () => {
        cy.visit('/');

        cy.contains('Ya haven\'t searched for anything else...');
        cy.contains('Yep...Nothin\'...');

        cy.get('input').type('luke');
        cy.get('button').click();

        cy.contains('Luke Skywalker', { matchCase: false }).click();

        cy.contains('About Luke Skywalker', { matchCase: false });

        cy.get('input').type('yo');
        cy.get('button').click();

        // cy.contains('luke', { matchCase: false });
        cy.contains('Yoda', { matchCase: false });
    });
})

export {}