import { appStore, StateProvider} from "../../src/utilities/State";
import React, {useContext} from "react";
import Previous from "../../components/tables/Previous";

function MockComponent() {
    const globalState = useContext(appStore);
    const { dispatch } = globalState;

    function handleChange(value: string) {
        dispatch({ type: 'search/update', value: value + ':'});
    }

    return <input onChange={(e) => handleChange(e.target.value)} />
}

describe('Previous Search', () => {
    beforeEach(() => {
        cy.mount(<StateProvider>
            <Previous />
            <MockComponent />
        </StateProvider>)
        cy.get('input').clear();
    });

    it('should have no results', () => {
        cy.contains('Ya haven\'t searched for anything else...');
    });

    it('should have a result', () => {
        cy.get('input').type('a');
        cy.get('input').type('b');
        cy.get('td').contains('a');
    });

    it('should have only 5 results', () => {
        cy.get('input').type('a');
        cy.get('input').type('b');
        cy.get('input').type('c');
        cy.get('input').type('d');
        cy.get('input').type('e');
        cy.get('input').type('f');
        cy.get('input').type('g');

        cy.get('td').contains('a:').should('not.exist');
    });
})

export {}