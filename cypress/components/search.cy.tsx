import Search from '../../components/inputs/Search';
import { appStore, StateProvider} from "../../src/utilities/State";
import React, {useContext} from "react";
import {RouterContext} from "next/dist/shared/lib/router-context";

function MockComponent() {
    const globalState = useContext(appStore);
    return <p>{globalState.searchValue}</p>
}

const createRouter = () => {
    return {
        pathname: '/',
        route: '/',
        query: {},
        asPath: '/',
        components: {},
        isFallback: false,
        basePath: '',
        events: { emit: cy.spy().as('emit'), off: cy.spy().as('off'), on: cy.spy().as('on') },
        push: cy.spy().as('push'),
        replace: cy.spy().as('replace'),
        reload: cy.spy().as('reload'),
        back: cy.spy().as('back'),
        prefetch: cy.stub().as('prefetch').resolves(),
        beforePopState: cy.spy().as('beforePopState'),
    }
}


describe('Search', () => {
    beforeEach(() => {
        const router = createRouter();
        cy.mount(<RouterContext.Provider value={router}>
            <StateProvider>
            <Search />
            <MockComponent />
        </StateProvider>
    </RouterContext.Provider>
    )
        cy.get('input').clear();
    });

    it('should show error', () => {
        cy.get('button').click();
        cy.get('span').contains('Please enter a search value.');
    });

    it('should update on click', () => {
        cy.get('input').type('my awesome name');
        cy.get('button').click();
        cy.get('p').contains('my awesome name');
    });

    it('should update on enter', () => {
        cy.get('input').type('something{enter}');
        cy.get('p').contains('something');
    });
})

export {}