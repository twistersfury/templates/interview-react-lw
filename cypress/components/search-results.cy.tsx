import { appStore, StateProvider} from "../../src/utilities/State";
import React, {useContext} from "react";
import Swapi from "../../src/utilities/Swapi";
import Character from "../../src/models/Character";
import SearchResults from "../../components/tables/SearchResults";

function MockComponent() {
    const globalState = useContext(appStore);
    const { dispatch } = globalState;

    function handleClick() {
        dispatch({ type: 'search/update', value: 'a'});
    }

    return <button onClick={handleClick} />
}

describe('Search Results', () => {
    let stubApi: any;
    beforeEach(() => {
        stubApi = cy.stub(Swapi, 'searchCharacters');

        stubApi.returns(
            new Promise((resolve) => {
                resolve([]);
            })
        );

        cy.mount(<StateProvider>
            <SearchResults />
            <MockComponent />
        </StateProvider>)
    });

    it('should have no results', () => {
        cy.get('button').click();
        cy.contains('Yep...Nothin\'...');
    });

    it('should have a result', () => {
        stubApi.withArgs('a').returns(
            new Promise((resolve) => {
                alert('here');
                let character = new Character({name: "Blah", url: "20"});
                resolve([character]);
            })
        );

        cy.get('button').click();
        cy.get('td').contains('Blah');
    });
})

export {}