// Nothing To See Here...For Now...

import { mount } from 'cypress/react18'
import {ReactNode} from "react";

Cypress.Commands.add("mount", (component: ReactNode, options) => {
    return mount(component, options);
});

export {}