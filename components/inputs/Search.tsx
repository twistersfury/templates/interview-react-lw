import styles from "../../scss/components/search.module.scss";
import { appStore } from "../../src/utilities/State";
import {useContext, useState} from "react";
import {BiSearch} from "react-icons/bi";
import {Button, Col, Form, InputGroup, Row} from "react-bootstrap";
import { useRouter } from "next/router";

const Search = () => {
    const [currentSearch, updateSearch] = useState('');
    const [isValid, updateValid] = useState(true);

    const globalState = useContext(appStore);
    const { dispatch } = globalState;
    const router = useRouter();

    function handleKey(e) {
        if (e.key === 'Enter') {
            handleSearch();
        }
    }

    function handleSearch() {
        updateValid(currentSearch != '');

        if (currentSearch != '') {
            dispatch({ type: 'search/update', value: currentSearch});
            updateSearch('');
            router.push("/");
        }
    }

    return <Row>
        <Col xs="auto">
            <InputGroup>
                <Form.Control className={styles.searchField} onKeyDown={handleKey} value={currentSearch} onChange={(e) => updateSearch(e.target.value)} placeholder="Character Search" />
            </InputGroup>
        </Col>
        <Col xs="auto">
            <Button type="button" onClick={handleSearch}>Search <BiSearch /></Button>
        </Col>
        <Col xs="auto">
            {!isValid && (
                <span className={styles.error}>Please enter a search value.</span>
            )}
        </Col>
    </Row>
}

export default Search;