import {PropsWithChildren, useContext, useEffect, useState} from "react";
import Head from 'next/head';
import Header from "./Header";

import style from "../scss/global/layout.module.scss"
import {appStore} from "../src/utilities/State";

const Layout = (props: PropsWithChildren) => {
    const displayName = "Layout";

    const [ appTitle, updateTitle ] = useState('// SWAPI')
    const globalState = useContext(appStore);

    useEffect(() => {
        let title = "// SWAPI";
        if (globalState.currentCharacter != null) {
            title += " // " + globalState.currentCharacter.name;
        }

        updateTitle(title);
    }, [globalState]);

    return <div className={style.layout}>
        <Head>
            <title>{appTitle}</title>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta charSet="utf-8" />
            <link
                rel="stylesheet"
                href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
                integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65"
                crossOrigin="anonymous"
            />
        </Head>
        <Header appTitle={appTitle} />

        <main className={style.content}>{ props.children }</main>
    </div>
};
export default Layout;