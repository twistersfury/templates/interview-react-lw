import Species from "../../src/models/Species";
import {useEffect, useState} from "react";

interface ISpeciesProps {
    species: Promise<Array<Species>>;
}

const SpeciesLayout = (props: ISpeciesProps) => {
    const [ species, updateSpecies ] = useState<Array<Species>>([]);

    useEffect(() => {
        props.species.then((loaded) => {
            updateSpecies(loaded);
        });

    }, [props.species]);

    return <ul>
        {
            species.map((specie: Species, index) => {
                return <li key={index}>{ specie.name }({specie.classification})</li>
            })
        }
    </ul>
};

export default SpeciesLayout;