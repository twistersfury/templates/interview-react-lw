import {useEffect, useState} from "react";
import Starship from "../../src/models/Starship";
import {Container, Table} from "react-bootstrap";

interface IShipsProps {
    ships: Promise<Array<Starship>>;
}

const StarshipLayout = (props: IShipsProps) => {
    const [ starships, updateShips ] = useState<Array<Starship>>([]);

    useEffect(() => {
        props.ships.then((loaded) => {
            updateShips(loaded);
        })
    }, [props.ships]);

    return <Container>
        <h3>Starships</h3>
        <Table  striped bordered hover>
            <thead>
            <tr>
                <th>Name</th>
                <th>Model</th>
            </tr>
            </thead>
            <tbody>
            {
                starships.map((ship: Starship, index) => {
                    return <tr key={ index }>
                        <td>{ ship.name }</td>
                        <td>{ ship.model }</td>
                    </tr>
                })
            }
            </tbody>
        </Table>
    </Container>
};

export default StarshipLayout;