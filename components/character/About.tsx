import {Container, Table} from "react-bootstrap";
import Species from "./Species";
import Character from "../../src/models/Character";

interface IAboutProps {
    character: Character;
}

const About = (props: IAboutProps) => {
    return <Container>
        <h3>About {props.character.name}</h3>
        <Table striped bordered hover>
            <tbody>
            <tr>
                <td>Height</td>
                <td>{props.character.height}</td>
            </tr>
            <tr>
                <td>Weight</td>
                <td>{props.character.weight}</td>
            </tr>
            <tr>
                <td>Hair Color</td>
                <td>{props.character.hairColor}</td>
            </tr>
            <tr>
                <td>Date of Birth</td>
                <td>{props.character.birthDate}</td>
            </tr>
            <tr>
                <td>Species</td>
                <td>
                    <Species species={props.character.getSpecies()} />
                </td>
            </tr>
            </tbody>
        </Table>
    </Container>
};

export default About;