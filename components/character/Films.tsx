import {useEffect, useState} from "react";
import Film from "../../src/models/Film";
import {Container, Table} from "react-bootstrap";

interface IFilmsProps {
    films: Promise<Array<Film>>;
}

const FilmLayout = (props: IFilmsProps) => {
    const [ films, updateFilms ] = useState<Array<Film>>([]);

    useEffect(() => {
        props.films.then((loaded) => {
            updateFilms(loaded);
        })
    }, [props.films]);

    return <Container>
        <h3>Films</h3>
        <Table  striped bordered hover>
            <thead>
            <tr>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
            {
                films.map((Film: Film, index) => {
                    return <tr key={ index }>
                        <td>{ Film.title }</td>
                    </tr>
                })
            }
            </tbody>
        </Table>
    </Container>
};

export default FilmLayout;