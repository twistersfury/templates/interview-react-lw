import {PropsWithChildren} from "react";
import styles from '../scss/global/header.module.scss';
import Search from "../components/inputs/Search";

interface IHeaderProps extends PropsWithChildren {
    appTitle: string;
}

const Header = (props: IHeaderProps) => {
    const displayName = "Header";

    return <header className={styles.header}>
        <span className={styles.appTitle}>{props.appTitle}</span>
        <Search />
    </header>
};

export default Header;