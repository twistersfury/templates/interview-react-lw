import {Component, PropsWithChildren} from "react";
import styles from "../scss/global/footer.module.scss";
class Footer extends Component<PropsWithChildren> {
    render() {
        return <footer className={styles.footer}>Footer</footer>
    }
}

export default Footer;