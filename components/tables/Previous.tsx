import {Col, Container, Row, Table} from "react-bootstrap";
import {useContext, useEffect, useState} from "react";
import {appStore} from "../../src/utilities/State";
import logger from "../../src/utilities/Logger";

const Previous = () => {
    const globalState = useContext(appStore);
    const { dispatch } = globalState;

    const [searches, updateSearches] = useState([]);

    useEffect(() => {
        if (globalState.searchValue == '') {
            return;
        }

        let newSearches = searches;

        newSearches.unshift(globalState.searchValue);
        if (newSearches.length > 5) {
            newSearches.pop();
        }

        logger.debug("Search Update", newSearches);

        updateSearches(newSearches);
    }, [globalState, searches]);

    function rerunSearch(searchValue: string) {
        if (searchValue == '') {
            return;
        }

        dispatch({ type: 'search/update', value: searchValue});
    }

    return <Container>
        <Row>
            <Col><h3>Previous Searches</h3></Col>
        </Row>
        <Row>
            <Col>
                <Table size="sm" striped bordered hover>
                    <thead>
                    <tr>
                        <th>Search</th>
                    </tr>
                    </thead>
                    <tbody>
                    { searches.length == 0 && (
                        <tr><td>Ya haven&apos;t searched for anything else...</td></tr>
                    )}
                    { searches.length > 0 &&
                        searches.map((name, index) => {
                            return <tr key={index}>
                                <td onClick={ () => rerunSearch(name) }>{ name }</td>
                            </tr>
                        })
                    }
                    </tbody>
                </Table>
            </Col>
        </Row>
    </Container>
};
export default Previous;