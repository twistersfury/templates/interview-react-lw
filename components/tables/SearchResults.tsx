import {Col, Container, Row, Table} from "react-bootstrap";
import {useContext, useEffect, useState} from "react";
import {appStore} from "../../src/utilities/State";
import Character from "../../src/models/Character";
import swapi from "../../src/utilities/Swapi";
import logger from "../../src/utilities/Logger";
import Link from "next/link";

const SearchResults = () => {
    const globalState = useContext(appStore);
    const [ results, updateResults ] = useState([]);

    useEffect(() => {
        if (globalState.searchValue == '') {
            return;
        }

        swapi.searchCharacters(globalState.searchValue).then(
            (results: Array<Character>) => {
                logger.debug("Search Results", results);

                updateResults(results);
            }
        );
    }, [globalState]);

    return <Container>
        <Row>
            <Col><h3>Search Results For {globalState.searchValue}</h3></Col>
        </Row>
        <Row>
            <Col>
                <Table size="sm" striped bordered hover>
                    <thead>
                    <tr>
                        <th>Character</th>
                    </tr>
                    </thead>
                    <tbody>
                    { results.length == 0 && (
                        <tr><td>Yep...Nothin&apos;...</td></tr>
                    )}
                    { results.length > 0 &&
                        results.map((character: Character, index) => {
                            return <tr key={index}>
                                <td><Link href={`/character/${character.id}`}>{ character.name }</Link></td>
                            </tr>
                        })
                    }
                    </tbody>
                </Table>
            </Col>
        </Row>
    </Container>
};

export default SearchResults;