## Lifeway React App

#### Installation

1. Insure you've got `GNUMake` and `docker` or `rancher-desktop` (in moby configuration) installed.
2. run `make node`
3. run `make npm-dev`
4. Navigate to `http://interview.localhost:3000`

#### Running Tests

1. `make jest` - Run Unit Tests
2. `make cypress-run-component` - Run Cypress Components
3. `make cypress-run` - Run Cypress E2E

#### Running Cleanup
1. `make lint` - Run ESLint