import Layout from "../components/Layout";
import Previous from "../components/tables/Previous";
import SearchResults from "../components/tables/SearchResults";

const Index = () => {
    const displayName = "Layout Component";

    return <Layout>
        <Previous />
        <SearchResults />
    </Layout>
};

export default Index;