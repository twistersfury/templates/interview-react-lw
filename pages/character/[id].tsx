import Layout from "../../components/Layout"
import {useRouter} from "next/router";
import {NextPage} from "next";
import {Col, Container, Table, Row} from "react-bootstrap";
import {useEffect, useState} from "react";
import Character from "../../src/models/Character";
import swapi from "../../src/utilities/Swapi";
import About from "../../components/character/About";
import Starships from "../../components/character/Starships";
import Starship from "../../src/models/Starship";
import Films from "../../components/character/Films";

const CharacterPage: NextPage = () => {
    const [currentCharacter, updateCharacter] = useState<Character|null>(null);
    const router = useRouter();
    const { id } = router.query;

    useEffect(() => {
        if (!id) {
            return;
        }

        swapi.loadCharacter(id).then(
            (character: Character) => {
                updateCharacter(character);
            }
        );
    }, [id]);

    if (!currentCharacter) {
        return <Layout><Container><Row><Col>Loading</Col></Row></Container></Layout>
    }

    return <Layout>
        <Container>
            <Row>
                <Col>
                    <About character={currentCharacter} />
                </Col>
                <Col>
                    <Films films={currentCharacter.getFilms()}/>
                </Col>
                <Col>
                    <Starships ships={currentCharacter.getStarships()}/>
                </Col>
            </Row>
        </Container>
    </Layout>
};

export default CharacterPage;