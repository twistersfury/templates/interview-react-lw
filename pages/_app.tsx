import '../scss/global.css'
import {PropsWithChildren} from "react";
import type { AppProps } from "next/app";
import { StateProvider } from "../src/utilities/State"

export default function App({
                                Component,
                                pageProps,
                            }: AppProps<PropsWithChildren>) {
    return <StateProvider>
        <Component {...pageProps} />
    </StateProvider>
}